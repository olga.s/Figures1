package by.stormnet.fxfigures.controllers;

import by.stormnet.fxfigures.Drawable.Drawable;
import by.stormnet.fxfigures.fxfigures.Figures;
import by.stormnet.fxfigures.fxfigures.Rectangle;
import by.stormnet.fxfigures.fxfigures.Round;
import by.stormnet.fxfigures.fxfigures.Triangle;
import com.sun.org.apache.xpath.internal.SourceTree;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MainWindowController {
    private List<Drawable> figures = new ArrayList<>();
    private Random random = new Random(System.currentTimeMillis());
    @FXML
    private Canvas canvas;

    @FXML
    private void onMouseClicked(MouseEvent event) {
        addFigure(event.getX(),event.getY(), random.nextInt(3));
         }

    private void addFigure(double x, double y, int type) {
        Figures figure = null;

        switch (type) {
            case Figures.FIGURE_TYPE_TRIANGLE:
                figure = new Triangle(x, y, random.nextInt(100), random.nextInt(100));
                break;
            case Figures.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(x, y, random.nextInt(100), random.nextInt(100));
                break;
            case Figures.FIGURE_TYPE_ROUND:
                Round round = new Round(x, y, random.nextInt(100));
                figures.add(round);
                repaint();
                return;
            default:
                System.out.println("unknown figure type!: " + type);
                break;
        }

        figures.add(figure);
        repaint();
    }

    private void repaint(){
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for (Drawable figure : figures) {
            figure.draw(canvas.getGraphicsContext2D());
        }
    }
}
