package by.stormnet.fxfigures.fxfigures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;


public class Rectangle extends Figures {
    private double width;
    private double height;


    public Rectangle(double cX, double cY) {

        super(Figures.FIGURE_TYPE_RECTANGLE, cX, cY, Color.BLACK, 4);
    }


    public Rectangle(double cX, double cY, double width, double height) {
        this(cX, cY);
        this.width = width;
        this.height = height;
        setPoints();
    }


    public void setDimentions(double width, double height) {
        this.width = width;
        this.height = height;
        setPoints();
    }

    private void setPoints(){
        getPoints().addAll(getcX() - width / 2,
                getcY() - height / 2,
                getcX() + width / 2,
                getcY() - height / 2,
                getcX() + width / 2,
                getcY() + height / 2,
                getcX() - width / 2,
                getcY() + height / 2);
    }


}
