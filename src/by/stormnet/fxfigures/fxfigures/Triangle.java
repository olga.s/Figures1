package by.stormnet.fxfigures.fxfigures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Triangle extends Figures {
    private double b;
    private double c;
    private double h;


    public Triangle(double cX, double cY) {
        super(Figures.FIGURE_TYPE_TRIANGLE, cX, cY, Color.PURPLE,3);
    }

    public Triangle(double cX, double cY, double c, double b) {
        this(cX, cY);
        this.b = b;
        this.c = c;
        double h = Math.sqrt(Math.pow(c, 2) - Math.pow(b, 2));


        setPoint();
    }

    private void setPoint() {

        getPoints().addAll(getcX(),
                getcY() - h / 3 * 2,
                getcX() + b / 2 * 2,
                getcY() + h / 3,
                getcX() - b / 2,
                getcY() + h / 2);

    }


}