package by.stormnet.fxfigures.fxfigures;

import by.stormnet.fxfigures.Drawable.Drawable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;


public class Round implements Drawable{
    private double cX, cY, radius;
    private Color color = Color.YELLOW;

    public Round(double cX, double cY, double radius) {
        this.cX = cX;
        this.cY = cY;
        this.radius = radius;

    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
graphicsContext.setStroke(color);
graphicsContext.setLineWidth(2.0);
graphicsContext.strokeOval(cX - radius, cY - radius, radius * 2, radius * 2);
    }
}

