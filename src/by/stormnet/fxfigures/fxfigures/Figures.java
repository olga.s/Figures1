package by.stormnet.fxfigures.fxfigures;

import by.stormnet.fxfigures.Drawable.Drawable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.*;
import javafx.scene.shape.Polygon;


public abstract class Figures extends Polygon implements Drawable {
    public static final int FIGURE_TYPE_TRIANGLE = 0;
    public static final int FIGURE_TYPE_RECTANGLE = 1;
    public static final int FIGURE_TYPE_ROUND = 2;

    private int type;
    private double cX;
    private double cY;
    private Color color;
    private int vertexCount;


    public Figures(int type, double cX, double cY, Color color, int vertexCount) {
        this(type);
        this.cX = cX;
        this.cY = cY;
        this.color = color;
        this.vertexCount = vertexCount;
    }


    public double getcX() {
        return cX;
    }

    public void setcX(double cX) {
        this.cX = cX;
    }

    public double getcY() {
        return cY;
    }

    public void setcY(double cY) {
        this.cY = cY;
    }

    public Figures(int type) {
        super();
        this.type = type;
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
        if (graphicsContext == null) {
            return;
        }
        graphicsContext.setStroke(color);
        graphicsContext.setLineWidth(2.0);
        double[] xCoords = new double[vertexCount];
        double[] yCoords = new double[vertexCount];
        int xindex = 0;
        int yindex = 0;
        for (int i = 0; i <getPoints().size(); i++){
            if (i % 2 == 0) {
                xCoords[xindex] = getPoints().get(i).doubleValue();
                xindex++;
            } else {
                yCoords[yindex] = getPoints().get(i).doubleValue();
                yindex++;
            }
        }
        graphicsContext.strokePolygon(xCoords, yCoords, vertexCount);
    }
}
