package by.stormnet.fxfigures.Drawable;

import javafx.scene.canvas.GraphicsContext;

/**
 * Created by java4 on 14.5.17.
 */
public interface Drawable {
    void draw(GraphicsContext graphicsContext);



}
